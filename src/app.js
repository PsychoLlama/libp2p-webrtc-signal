import 'babel-polyfill';
import P2P from 'libp2p';
import { NOISE } from 'libp2p-noise';
import MPLEX from 'libp2p-mplex';
import WebRTCStar from 'libp2p-webrtc-star';
import Websockets from 'libp2p-websockets';
import pipe from 'it-pipe';
import { encode, decode } from 'it-length-prefixed';

async function main() {
  const p2p = await P2P.create({
    addresses: {
      listen: [
        '/dns4/localhost/tcp/9090/ws/p2p-webrtc-star/',
      ],
    },
    modules: {
      transport: [Websockets, WebRTCStar],
      connEncryption: [NOISE],
      streamMuxer: [MPLEX],
    },
  });

  p2p.connectionManager.on('peer:connect', (conn) => {
    console.log('New peer:', conn.remotePeer.toB58String());
    global.dial = dial.bind(null, p2p, conn);
  });

  p2p.connectionManager.on('peer:disconnect', (conn) => {
    console.log('Lost peer:', conn.remotePeer.toB58String());
  });

  p2p.handle('/test', (conn) => {
    console.log('New connection:', conn);

    pipe(conn.stream, decode(), async (source) => {
      for await (const chunk of source) {
        const buffer = chunk.slice();
        const msg = new TextDecoder().decode(buffer);
        console.log('->', msg);
      }
    });
  });

  await p2p.start();

  return p2p;
}

async function dial(p2p, entry) {
  const madr = `/p2p/${entry.remotePeer.toB58String()}`;
  const conn = await p2p.dialProtocol(madr, '/test');
  pipe(['hello', 'world'], encode(), conn.stream);
  return conn;
}

main();
