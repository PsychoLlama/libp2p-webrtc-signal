const P2P = require('libp2p');
const { NOISE } = require('libp2p-noise');
const TCP = require('libp2p-tcp');
const WebRTCStar = require('libp2p-webrtc-star');
const Websockets = require('libp2p-websockets');
const MPLEX = require('libp2p-mplex');
const MDNS = require('libp2p-mdns');
const pipe = require('it-pipe');
const { decode } = require('it-length-prefixed');
const wrtc = require('wrtc');

const WRTC_TAG = WebRTCStar.prototype[Symbol.toStringTag];

async function main() {
  const p2p = await P2P.create({
    addresses: {
      listen: [
        '/dns4/localhost/tcp/9090/ws/p2p-webrtc-star/',
        '/ip4/0.0.0.0/tcp/0/ws',
      ],
    },
    modules: {
      transport: [TCP, Websockets, WebRTCStar],
      connEncryption: [NOISE],
      streamMuxer: [MPLEX],
      peerDiscovery: [MDNS],
    },
    config: {
      transport: {
        [WRTC_TAG]: { wrtc },
      },
      peerDiscovery: {
        [WRTC_TAG]: { enabled: true },
      },
      relay: {
        enabled: true,
        hop: {
          enabled: true,
        },
      },
    },
  });

  p2p.connectionManager.on('peer:connect', (conn) => {
    console.log('New peer:', conn.remotePeer.toB58String());
  });

  p2p.connectionManager.on('peer:disconnect', (conn) => {
    console.log('Lost peer:', conn.remotePeer.toB58String());
  });

  p2p.handle('/test', (conn) => {
    pipe(conn.stream, decode(), async (source) => {
      for await (const chunk of source) {
        const buffer = chunk.slice();
        const msg = new TextDecoder().decode(buffer);
        console.log('->', msg);
      }
    });
  });

  await p2p.start();

  p2p.multiaddrs.forEach((madr) => {
    console.log(`- ${madr}/p2p/${p2p.peerId.toB58String()}`);
  });
}

main();
