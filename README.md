# libp2p-webrtc-signal
Shows how to set up a minimal client and server for connecting browsers with libp2p over WebRTC.

## Setup

1. `yarn install`
2. `yarn start:server`
3. `yarn start:web`

Open two browser tabs, open the consoles, then watch the magic happen. Optionally run the node-client module to add a Node.js peer in the mix.
